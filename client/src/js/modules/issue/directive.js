angular.module('laboard-frontend')
    .directive('collapsible', [
        function() {
            return {
                restrict: 'A',
                link: function($scope, $element, $attrs) {
                    var panel = $('.panel-body', $element),
                        body = $('.issue-body', panel);

                    $element.hover(
                        function() {
                            panel.css('max-height', body.outerHeight());
                        },
                        function() {
                            panel.css('max-height', '');
                        }
                    );
                }
            };
        }
    ])
    .directive('draggable', [
        function() {
            return {
                restrict: 'A',
                link: function($scope, $element, $attrs) {
                    var width = $element.outerWidth(),
                        setWidth = function() {
                            $element.css('width', width);
                        },
                        unsetWidth = function() {
                            $element.css('width', '');
                        };

                    $element.on('mousedown', setWidth);
                    $element.on('mouseup', unsetWidth);

                    $scope.$on('$destroy', function() {
                        $element.off('mousedown', setWidth);
                        $element.off('mouseup', unsetWidth);
                    });
                }
            };
        }
    ]);
