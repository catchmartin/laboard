var q = require('q'),
    gitlab = module.exports = function gitlab(url, http) {
        this.base = url;
        this.http = http;
    };

gitlab.prototype = {
    login: function(username, password) {
        var options = {
            method: 'POST',
            uri: this.base + '/session',
            body: JSON.stringify({
                login: username,
                password: password
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        return this.http(options)
            .then(function(response) {
                if (response.statusCode !== 201) {
                    throw response;
                }

                return JSON.parse(response.body);
            });
    },
    auth: function(token) {
        return this.http.get(this.base + '/user?private_token=' + token)
            .then(function(response) {
                if (response.statusCode !== 200) {
                    throw response;
                }

                return JSON.parse(response.body);
            });
    },
    url: function(token, url, params) {
        var query = '',
            sep = '?';

        if (!params) {
            params = [];
        }

        params.private_token = token;

        Object.keys(params).forEach(function (key) {
            query += sep + key + '=' + params[key];
            sep = '&';
        });

        return this.base + url + query;
    },
    get: function(token, url, params) {
        return this.http.get(this.url(token, url, params))
            .then(function(response) {
                response.links = {};

                if (response.headers.link) {
                    response.headers.link.split(',')
                        .filter(function(value) {
                            return /<([^>]+)>; rel="([^"]+)"/.test(value);
                        })
                        .map(function(value) {
                            var matches = /<([^>]+)>; rel="([^"]+)"/.exec(value);

                            return [matches[1], matches[2]];
                        })
                        .forEach(function(link) {
                            response.links[link[1]] = link[0];
                        });
                }

                try {
                    response.body = JSON.parse(response.body);
                } catch (e) {}

                return response;
            });
    },
    put: function(token, url, body, params) {
        var options = {
            method: 'PUT',
            uri: this.url(token, url, params),
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        return this.http(options)
            .then(function(response) {
                try {
                    response.body = JSON.parse(response.body);
                } catch (e) {}

                return response;
            });
    },
    post: function(token, url, body, params) {
        var options = {
            method: 'POST',
            uri: this.url(token, url, params),
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        return this.http(options)
            .then(function(response) {
                try {
                    response.body = JSON.parse(response.body);
                } catch (e) {}

                return response;
            });
    }
};
